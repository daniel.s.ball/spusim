module ThreeBitCounter( clk, clk1, clk2, clk4);
	input clk;
	output clk1, clk2, clk4;
	
	assign clk1 = clk;
	halver halver1(clk1, clk2);
	halver halver2(clk2, clk4);
endmodule