transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/git/spusim/Modded_Source/test {C:/git/spusim/Modded_Source/test/dflipflop.v}
vlog -vlog01compat -work work +incdir+C:/git/spusim/Modded_Source/test {C:/git/spusim/Modded_Source/test/four_bank_mem.v}
vlog -vlog01compat -work work +incdir+C:/git/spusim/Modded_Source/test {C:/git/spusim/Modded_Source/test/final_memory.syn.v}

