onerror {exit -code 1}
vlib work
vlog -work work spusim.vo
vlog -work work test.vwf.vt
vsim -novopt -c -t 1ps -L cycloneive_ver -L altera_ver -L altera_mf_ver -L 220model_ver -L sgate work.four_bank_mem_vlg_vec_tst -voptargs="+acc"
vcd file -direction spusim.msim.vcd
vcd add -internal four_bank_mem_vlg_vec_tst/*
vcd add -internal four_bank_mem_vlg_vec_tst/i1/*
run -all
quit -f
