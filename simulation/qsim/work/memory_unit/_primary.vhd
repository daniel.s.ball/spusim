library verilog;
use verilog.vl_types.all;
entity memory_unit is
    port(
        read_data       : out    vl_logic_vector(15 downto 0);
        busy            : out    vl_logic;
        error           : out    vl_logic;
        write_data      : in     vl_logic_vector(15 downto 0);
        addr            : in     vl_logic_vector(12 downto 0);
        write           : in     vl_logic;
        en              : in     vl_logic;
        clk             : in     vl_logic
    );
end memory_unit;
