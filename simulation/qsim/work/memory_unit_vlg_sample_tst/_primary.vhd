library verilog;
use verilog.vl_types.all;
entity memory_unit_vlg_sample_tst is
    port(
        addr            : in     vl_logic_vector(12 downto 0);
        clk             : in     vl_logic;
        en              : in     vl_logic;
        write           : in     vl_logic;
        write_data      : in     vl_logic_vector(15 downto 0);
        sampler_tx      : out    vl_logic
    );
end memory_unit_vlg_sample_tst;
