library verilog;
use verilog.vl_types.all;
entity ThreeBitCounter is
    port(
        clk             : in     vl_logic;
        clk1            : out    vl_logic;
        clk2            : out    vl_logic;
        clk4            : out    vl_logic
    );
end ThreeBitCounter;
