library verilog;
use verilog.vl_types.all;
entity ThreeBitCounter_vlg_check_tst is
    port(
        clk1            : in     vl_logic;
        clk2            : in     vl_logic;
        clk4            : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end ThreeBitCounter_vlg_check_tst;
