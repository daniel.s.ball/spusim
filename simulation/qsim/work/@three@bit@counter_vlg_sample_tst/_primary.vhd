library verilog;
use verilog.vl_types.all;
entity ThreeBitCounter_vlg_sample_tst is
    port(
        clk             : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end ThreeBitCounter_vlg_sample_tst;
