library verilog;
use verilog.vl_types.all;
entity memory_unit_vlg_check_tst is
    port(
        busy            : in     vl_logic;
        error           : in     vl_logic;
        read_data       : in     vl_logic_vector(15 downto 0);
        sampler_rx      : in     vl_logic
    );
end memory_unit_vlg_check_tst;
