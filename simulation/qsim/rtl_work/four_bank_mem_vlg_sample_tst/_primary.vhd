library verilog;
use verilog.vl_types.all;
entity four_bank_mem_vlg_sample_tst is
    port(
        addr            : in     vl_logic_vector(15 downto 0);
        clk             : in     vl_logic;
        createdump      : in     vl_logic;
        data_in         : in     vl_logic_vector(15 downto 0);
        rd              : in     vl_logic;
        rst             : in     vl_logic;
        wr              : in     vl_logic;
        sampler_tx      : out    vl_logic
    );
end four_bank_mem_vlg_sample_tst;
