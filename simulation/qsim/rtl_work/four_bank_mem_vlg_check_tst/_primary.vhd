library verilog;
use verilog.vl_types.all;
entity four_bank_mem_vlg_check_tst is
    port(
        busy            : in     vl_logic_vector(3 downto 0);
        data_out        : in     vl_logic_vector(15 downto 0);
        err             : in     vl_logic;
        stall           : in     vl_logic;
        sampler_rx      : in     vl_logic
    );
end four_bank_mem_vlg_check_tst;
