transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+C:/git/spusim/Modded_Source {C:/git/spusim/Modded_Source/dflipflop.v}
vlog -vlog01compat -work work +incdir+C:/git/spusim/Modded_Source {C:/git/spusim/Modded_Source/busy_check.v}

