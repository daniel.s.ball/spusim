onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate /busy_check/d
add wave -noupdate /busy_check/clk
add wave -noupdate /busy_check/busy
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {2 ps} 0}
quietly wave cursor active 1
configure wave -namecolwidth 173
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ps
update
WaveRestoreZoom {0 ps} {973 ps}
view wave 
wave clipboard store
wave create -pattern none -portmode input -language vlog /busy_check/d 
wave create -pattern none -portmode input -language vlog /busy_check/clk 
wave create -pattern none -portmode input -language vlog /busy_check/rst 
wave create -pattern none -portmode output -language vlog /busy_check/busy 
wave modify -driver freeze -pattern clock -initialvalue (no value) -period 100ps -dutycycle 50 -starttime 0ps -endtime 1000ps Edit:/busy_check/clk 
wave modify -driver freeze -pattern constant -value (no value) -starttime 0ps -endtime 1000ps Edit:/busy_check/d 
wave modify -driver freeze -pattern constant -value 0 -starttime 0ps -endtime 1000ps Edit:/busy_check/d 
wave modify -driver freeze -pattern random -initialvalue (no value) -period 50ps -random_type Uniform -seed 5 -starttime 0ps -endtime 1000ps Edit:/busy_check/d 
wave modify -driver freeze -pattern random -initialvalue 0 -period 50ps -random_type Normal -seed 48563 -starttime 0ps -endtime 1000ps Edit:/busy_check/d 
WaveCollapseAll -1
wave clipboard restore
