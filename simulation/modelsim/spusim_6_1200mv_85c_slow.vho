-- Copyright (C) 1991-2013 Altera Corporation
-- Your use of Altera Corporation's design tools, logic functions 
-- and other software and tools, and its AMPP partner logic 
-- functions, and any output files from any of the foregoing 
-- (including device programming or simulation files), and any 
-- associated documentation or information are expressly subject 
-- to the terms and conditions of the Altera Program License 
-- Subscription Agreement, Altera MegaCore Function License 
-- Agreement, or other applicable license agreement, including, 
-- without limitation, that your use is for the sole purpose of 
-- programming logic devices manufactured by Altera and sold by 
-- Altera or its authorized distributors.  Please refer to the 
-- applicable agreement for further details.

-- VENDOR "Altera"
-- PROGRAM "Quartus II 64-Bit"
-- VERSION "Version 13.1.0 Build 162 10/23/2013 SJ Web Edition"

-- DATE "05/20/2017 19:05:35"

-- 
-- Device: Altera EP4CE22F17C6 Package FBGA256
-- 

-- 
-- This VHDL file should be used for ModelSim-Altera (VHDL) only
-- 

LIBRARY ALTERA;
LIBRARY CYCLONEIVE;
LIBRARY IEEE;
USE ALTERA.ALTERA_PRIMITIVES_COMPONENTS.ALL;
USE CYCLONEIVE.CYCLONEIVE_COMPONENTS.ALL;
USE IEEE.STD_LOGIC_1164.ALL;

ENTITY 	busy_check IS
    PORT (
	d : IN std_logic;
	clk : IN std_logic;
	rst : IN std_logic;
	busy : BUFFER std_logic
	);
END busy_check;

-- Design Ports Information
-- busy	=>  Location: PIN_K5,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- d	=>  Location: PIN_N1,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- rst	=>  Location: PIN_N2,	 I/O Standard: 2.5 V,	 Current Strength: Default
-- clk	=>  Location: PIN_E1,	 I/O Standard: 2.5 V,	 Current Strength: Default


ARCHITECTURE structure OF busy_check IS
SIGNAL gnd : std_logic := '0';
SIGNAL vcc : std_logic := '1';
SIGNAL unknown : std_logic := 'X';
SIGNAL devoe : std_logic := '1';
SIGNAL devclrn : std_logic := '1';
SIGNAL devpor : std_logic := '1';
SIGNAL ww_devoe : std_logic;
SIGNAL ww_devclrn : std_logic;
SIGNAL ww_devpor : std_logic;
SIGNAL ww_d : std_logic;
SIGNAL ww_clk : std_logic;
SIGNAL ww_rst : std_logic;
SIGNAL ww_busy : std_logic;
SIGNAL \clk~inputclkctrl_INCLK_bus\ : std_logic_vector(3 DOWNTO 0);
SIGNAL \busy~output_o\ : std_logic;
SIGNAL \clk~input_o\ : std_logic;
SIGNAL \clk~inputclkctrl_outclk\ : std_logic;
SIGNAL \rst~input_o\ : std_logic;
SIGNAL \d~input_o\ : std_logic;
SIGNAL \ff0|state~0_combout\ : std_logic;
SIGNAL \ff0|state~q\ : std_logic;
SIGNAL \ff1|state~0_combout\ : std_logic;
SIGNAL \ff1|state~q\ : std_logic;
SIGNAL \ff2|state~0_combout\ : std_logic;
SIGNAL \ff2|state~q\ : std_logic;
SIGNAL \busy~0_combout\ : std_logic;

BEGIN

ww_d <= d;
ww_clk <= clk;
ww_rst <= rst;
busy <= ww_busy;
ww_devoe <= devoe;
ww_devclrn <= devclrn;
ww_devpor <= devpor;

\clk~inputclkctrl_INCLK_bus\ <= (vcc & vcc & vcc & \clk~input_o\);

-- Location: IOOBUF_X0_Y7_N9
\busy~output\ : cycloneive_io_obuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	open_drain_output => "false")
-- pragma translate_on
PORT MAP (
	i => \busy~0_combout\,
	devoe => ww_devoe,
	o => \busy~output_o\);

-- Location: IOIBUF_X0_Y16_N8
\clk~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_clk,
	o => \clk~input_o\);

-- Location: CLKCTRL_G2
\clk~inputclkctrl\ : cycloneive_clkctrl
-- pragma translate_off
GENERIC MAP (
	clock_type => "global clock",
	ena_register_mode => "none")
-- pragma translate_on
PORT MAP (
	inclk => \clk~inputclkctrl_INCLK_bus\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	outclk => \clk~inputclkctrl_outclk\);

-- Location: IOIBUF_X0_Y8_N22
\rst~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_rst,
	o => \rst~input_o\);

-- Location: IOIBUF_X0_Y7_N1
\d~input\ : cycloneive_io_ibuf
-- pragma translate_off
GENERIC MAP (
	bus_hold => "false",
	simulate_z_as => "z")
-- pragma translate_on
PORT MAP (
	i => ww_d,
	o => \d~input_o\);

-- Location: LCCOMB_X1_Y7_N12
\ff0|state~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ff0|state~0_combout\ = (!\rst~input_o\ & \d~input_o\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0011000000110000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datab => \rst~input_o\,
	datac => \d~input_o\,
	combout => \ff0|state~0_combout\);

-- Location: FF_X1_Y7_N13
\ff0|state\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \ff0|state~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ff0|state~q\);

-- Location: LCCOMB_X1_Y7_N2
\ff1|state~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ff1|state~0_combout\ = (!\rst~input_o\ & \ff0|state~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \rst~input_o\,
	datad => \ff0|state~q\,
	combout => \ff1|state~0_combout\);

-- Location: FF_X1_Y7_N3
\ff1|state\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \ff1|state~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ff1|state~q\);

-- Location: LCCOMB_X1_Y7_N0
\ff2|state~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \ff2|state~0_combout\ = (!\rst~input_o\ & \ff1|state~q\)

-- pragma translate_off
GENERIC MAP (
	lut_mask => "0000111100000000",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	datac => \rst~input_o\,
	datad => \ff1|state~q\,
	combout => \ff2|state~0_combout\);

-- Location: FF_X1_Y7_N1
\ff2|state\ : dffeas
-- pragma translate_off
GENERIC MAP (
	is_wysiwyg => "true",
	power_up => "low")
-- pragma translate_on
PORT MAP (
	clk => \clk~inputclkctrl_outclk\,
	d => \ff2|state~0_combout\,
	devclrn => ww_devclrn,
	devpor => ww_devpor,
	q => \ff2|state~q\);

-- Location: LCCOMB_X1_Y7_N14
\busy~0\ : cycloneive_lcell_comb
-- Equation(s):
-- \busy~0_combout\ = (\ff0|state~q\) # ((\ff1|state~q\) # ((\d~input_o\) # (\ff2|state~q\)))

-- pragma translate_off
GENERIC MAP (
	lut_mask => "1111111111111110",
	sum_lutc_input => "datac")
-- pragma translate_on
PORT MAP (
	dataa => \ff0|state~q\,
	datab => \ff1|state~q\,
	datac => \d~input_o\,
	datad => \ff2|state~q\,
	combout => \busy~0_combout\);

ww_busy <= \busy~output_o\;
END structure;


