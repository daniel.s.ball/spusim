library verilog;
use verilog.vl_types.all;
entity busy_check is
    port(
        d               : in     vl_logic;
        clk             : in     vl_logic;
        rst             : in     vl_logic;
        busy            : out    vl_logic
    );
end busy_check;
