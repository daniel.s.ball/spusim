module halver( clk, clk2x );
	input clk;
	output reg clk2x;

	// toggle divided clock
	always @ (posedge clk) begin
		clk2x <= ~clk2x;
	end
endmodule