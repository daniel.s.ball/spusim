/* $Author: karu $ */
/* $LastChangedDate: 2009-03-04 23:09:45 -0600 (Wed, 04 Mar 2009) $ */
/* $Rev: 45 $ */
// Synthesizable memory
////////////////////////////////////////////////
//
// final_memory -- four cycle version of memory
// for use in the four-banked memory
//
// written for CS/ECE 552, Spring '06
// Andy Phelps, 1 May 2006
//
// modified 30 Oct 2006 by Derek Hower
//    - byte-addressable, word aligned
// modified 23 Apr 2008 by karu
//    - addr_1c must be 14 bits wide
// This is a word-addressable,
// 16-bit wide, 16K-byte memory.
//
//    |            |            |            |            |            |
//    | addr       | busy       | read data  |            | new addr   |
//    | data_in    |            |            |            | etc.       |
//    | wr, rd     |            |            |            |            |
//    | enable     |            |            |            |            |
//    |            |            |            |            |            |
//                  <----bank busy; do not try new req--->
//
// Requests may be presented at most every 4th cycle;
// a new request before this time will result in an error.
// Read requests presented in cycle N will deliver data in cycle N+2.
// Concurrent read and write not allowed.
//
// On reset, memory loads from file "loadfile_0.img",
// "loadfile_1.img", "loadfile_2.img", or "loadfile_3.img", depending
// on the "bank_id" input.
//
// File format:
//     @0
//     <hex data 0>
//     <hex data 1>
//     ...etc
//
// If input "create_dump" is true on rising clock,
// contents of memory will be dumped to
// file "dumpfile_0", "dumpfile_1", etc, depending on
// "bank_id".  It will dump from location 0 up through
// the highest location modified by a write.
//
// File names for loading and dumping is the only purpose of
// the "bank_id" input.  (You may change the name of the file
// in the $readmemh statement below.)
//
//////////////////////////////////////

module final_memory (
	output [15:0] read_data,
	output        err,
	input  [15:0] write_data,
	input  [12:0] addr,
	input         wr,
	input         rd,
	input         enable,
	input         clk,
	input         rst
);

    // 16 x 8191 = 8k words = 16 kB
	// reg     [15:0]  mem [0:8191];  	
	
	// 16 x 32 for testing
	reg [15:0] mem [0:31];	// set size of memory based on parameters

	assign rd0 = rd & ~wr & enable & ~rst;
	assign wr0 = ~rd & wr & enable & ~rst;
	 
//	dff reg0 [12:0] (addr_1c[12:0], addr, clk, rst);
//	dff reg1 [15:0] (data_in_1c, data_in, clk, rst);

//    wire [15:0] data_out_1c = rd1 ? {mem[addr], mem[(addr]} : {16{1'bz};
	assign data_out = (rd0 | rd1) ? mem[addr] : {16{1'bz}};

	dff reg2 [15:0] (data_out, data_out_1c, clk, rst);

	// handle single cycle busy
	dff ff0 (rd1, rd0, clk, rst);
	dff ff1 (wr1, wr0, clk, rst);
	
//    dff ff2 (rd2, rd1, clk, rst);
//    dff ff3 (wr2, wr1, clk, rst);
//    dff ff4 (rd3, rd2, clk, rst);
//    dff ff5 (wr3, wr2, clk, rst);

//    assign busy = rd1 | rd2 | rd3 | wr1 | wr2 | wr3;
	assign busy = rd1 | wr1;
	assign err = ((rd0 | wr0) & busy) | (rd & wr & enable & ~rst);


	// synchronous write
	always @(posedge clk) begin
		if (wr1) begin
			mem[addr] <= write_data;  // Write input data
		end
	end


endmodule  // final_memory
// DUMMY LINE FOR REV CONTROL :0:
