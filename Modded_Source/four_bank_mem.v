/* $Author: karu $ */
/* $LastChangedDate: 2009-03-04 23:09:45 -0600 (Wed, 04 Mar 2009) $ */
/* $Rev: 45 $ */
////////////////////////////////////////////////
//
// four_bank_mem -- four banks of the
// 4-cycle "final_memory" for use in the
// most advanced stage of the project
//
// written for CS/ECE 552, Spring '06
// Andy Phelps, 6 Mar 2006
//
// Modified 30 Oct 2006 by Derek Hower
//   - memory was made byte-addressable, word aligned
//   - added busy output for DRAM controller
//   - set err to high on unaligned access
// 
// Modified by Karu 05/03
// Added & (rd |wr ) to err
//
// This is a word-addressable,
// 16-bit wide, 64K-word memory.
//
//    |            |            |            |            |            |
//    | addr       | addr etc   | read data  |            | new addr   |
//    | data_in    | OK to any  | available  |            | etc. is    |
//    | wr, rd     |*diffferent*|            |            | OK to      |
//    | enable     | bank       |            |            | *same*     |
//    |            |            |            |            | bank       |
//                  <----bank busy; any new request to--->
//                       the *same* bank will stall
//
// Requests may be presented every cycle.
// They will be directed to one of the four banks depending
// on the least significant 2 bits of the address.
//
// Two requests to the same bank which are closer than cycles N and N+4
// will result in the second request not happening, and a "stall" output
// being generated.
//
// Busy output reflects the current status of each individual bank.
//
// Concurrent read and write not allowed.
//
// On reset, memory loads from file "loadfile_0.img",
// "loadfile_1.img", "loadfile_2.img", and "loadfile_3.img".
// Each file supplies every fourth word.
// (The latest version of the assembler generates
// these four files.)
//
// Format of each file:
//     @0
//     <hex data 0>
//     <hex data 1>
//     ...etc
//
// If input "create_dump" is true on rising clock,
// contents of memory will be dumped to
// file "dumpfile_0", "dumpfile_1", etc.
// Each file will be a dump from location 0 up through
// the highest location modified by a write in that bank.
//
//////////////////////////////////////

// attempts to read from each bank assigned
// if 

module four_bank_mem (
    input         clk,
    input         rst,
    input         createdump,
    input  [15:0] addr0,	// address selection wires for each lane
	input  [15:0] addr1,
	input  [15:0] addr2,
	input  [15:0] addr3,
    input  [15:0] data_in0,	// data in wires for each lane
	input  [15:0] data_in1,
	input  [15:0] data_in2,
	input  [15:0] data_in3,
    input  [3:0]  wr,		// read/write for each lane
    input  [3:0]  rd,               
    output [15:0] data_out0, // data out wires for each lane
	output [15:0] data_out1,
	output [15:0] data_out2,
	output [15:0] data_out3,
    output        stall,
    output [3:0]  busy,
    output        err
);

wire [15:0] data0_out, data1_out, data2_out, data3_out;

// flags indicating selected bank

// lane arbitration
assign bank_lane0 = addr0[2:1]
assign bank_lane1 = addr1[2:1]
assign bank_lane2 = addr2[2:1]
assign bank_lane3 = addr3[2:1]


assign sel0 = (addr[2:1] == 2'd0);		// this bank is legallyselected by one of the lanes
assign sel1 = (addr[2:1] == 2'd1);		// this bank is legallyselected by one of the lanes
assign sel2 = (addr[2:1] == 2'd2);		// this bank is legallyselected by one of the lanes
assign sel3 = (addr[2:1] == 2'd3);		// this bank is legallyselected by one of the lanes




// enable signals to each bank
wire [3:0] en;
assign en[0] = sel0 & ~busy[0] & (wr | rd);
assign en[1] = sel1 & ~busy[1] & (wr | rd);
assign en[2] = sel2 & ~busy[2] & (wr | rd);
assign en[3] = sel3 & ~busy[3] & (wr | rd);

// stall if write or read, not reset, and any of the selected banks are busy
assign stall = (wr | rd) & ~rst & ( (sel0 & busy[0])
                                   |(sel1 & busy[1])
                                   |(sel2 & busy[2])
                                   |(sel3 & busy[3]) );
   
// instantiate 4 banks of memory
// address is most significant 13 bits of addr
// bank determined by addr[2:1]
// addr[0] should always be 0
memory_unit m0 (data0_out, err0, data_in, addr[15:3], wr, rd, en[0], createdump, 2'd0, clk, rst);
memory_unit m1 (data1_out, err1, data_in, addr[15:3], wr, rd, en[1], createdump, 2'd1, clk, rst);
memory_unit m2 (data2_out, err2, data_in, addr[15:3], wr, rd, en[2], createdump, 2'd2, clk, rst);
memory_unit m3 (data3_out, err3, data_in, addr[15:3], wr, rd, en[3], createdump, 2'd3, clk, rst);

// data out for selected bank
assign data_out0 = data0_out | data1_out | data2_out | data3_out;
assign data_out1 = data0_out | data1_out | data2_out | data3_out;
assign data_out2 = data0_out | data1_out | data2_out | data3_out;
assign data_out3 = data0_out | data1_out | data2_out | data3_out;

// error if selected address is odd
assign err = (wr | rd) & (err0 | err1 | err2 | err3 | addr[0]==1); //word aligned; odd addresses are illegal

// busy signals are stored in a 4 lane x 3 bit shift register.
// busy signals are shifted each clock cycle, with new busy signals input by an active en for that lane
wire [3:0] bsy0, bsy1, bsy2;
dflipflop b0 [3:0] (bsy0, en,    clk, rst);
dflipflop b1 [3:0] (bsy1, bsy0, clk, rst);
dflipflop b2 [3:0] (bsy2, bsy1, clk, rst);

// write busy signals to busy wires
assign busy = bsy0 | bsy1 | bsy2;

endmodule
