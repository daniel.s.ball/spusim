// 3 bit shift register
// if any register or input are true, output busy
module port_status_check(
	input 	r,
	input 	w,
	input  	en,
	input 	clk,
	input 	rst,	
	output 	busy,
	output 	err
);
	// determine read, write, error
	
	// Memory Error:
	// 1) memory is busy and read or write are enabled
	// 2) read and write are active simultaneously
	assign r0 = r[0] & ~w[0] & en[0] & ~rst;	
    assign w0 = ~r & w & en & ~rst;
	assign err = ((rd | wr0) & busy) | (rd & wr & en & ~rst);
	
	// read busy shift registers
	dflipflop ff0 (r1, r0, clk, rst);
	dflipflop ff1 (r2, r1, clk, rst);
	dflipflop ff2 (r3, r2, clk, rst);
	
	// write busy shift registers
	dflipflop ff3 (w1, w0, clk, rst);
	dflipflop ff4 (w2, w1, clk, rst);
	dflipflop ff5 (w3, w2, clk, rst);
	
	// Memory is currently being accessed on this port
	assign busy =  r1 | r2 | r3 |  w1 | w2 | w3;
	
	
    
	
endmodule