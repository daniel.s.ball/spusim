/*
Author: Daniel Ball
For SPU SIM memory model
Last Updated: 5/24/2017

16kB, 2 byte word, word addressed memory

*/

module memory_unit (	
	output 	[15: 0] read_data,
	output busy,
	input 	[15: 0] write_data,
	input 	[12: 0] address,
	input 	en,
	input 	write,
	input 	clk,
	input 	rst
);	
	// 16 x 8191 = 8k words = 16 kB
	// reg     [15:0]  mem [0:8191];  	
	
	// 16 x 32 for testing
	reg [15:0] mem [0:31];	// set size of memory based on parameters
	
	// asynchronous read
	// read data is high impedance if chip is not selected
	// Otherwise it will read the address
	assign read_data = (en & ~write) ? mem[address] : {16{1'bz}}; 
	
	// set busy if a read or write occurred
	dflipflop busy1 (en_prev, en, clk, rst);
	assign busy = en_prev;
	
	// synchronous write
	always @(posedge clk) begin
		if (write & cs) begin
			mem[address] <= write_data;  // Write input data
		end
	end

endmodule  // final_memory

