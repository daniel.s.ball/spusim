module lane(
	input	[15:0]	addr,
	input			lane_stall,
	input	[15:0] 	data_in,
	input 	[15:0] 	data_bank0,
	input 	[15:0] 	data_bank1,
	input 	[15:0] 	data_bank2,
	input 	[15:0] 	data_bank3,
	output 	[15:0] 	data_out
);