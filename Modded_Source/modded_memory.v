/* $Author: karu $ */
/* $LastChangedDate: 2009-03-04 23:09:45 -0600 (Wed, 04 Mar 2009) $ */
/* $Rev: 45 $ */
// Synthesizable memory
////////////////////////////////////////////////
//
// final_memory -- four cycle version of memory
// for use in the four-banked memory
//
// written for CS/ECE 552, Spring '06
// Andy Phelps, 1 May 2006
//
// modified 30 Oct 2006 by Derek Hower
//    - byte-addressable, word aligned
// modified 23 Apr 2008 by karu
//    - addr_1c must be 14 bits wide
// This is a word-addressable,
// 16-bit wide, 16K-byte memory.
//
//    |            |            |            |            |            |
//    | addr       | busy       | read data  |            | new addr   |
//    | data_in    |            |            |            | etc.       |
//    | wr, rd     |            |            |            |            |
//    | enable     |            |            |            |            |
//    |            |            |            |            |            |
//                  <----bank busy; do not try new req--->
//
// Requests may be presented at most every 4th cycle;
// a new request before this time will result in an error.
// Read requests presented in cycle N will deliver data in cycle N+2.
// Concurrent read and write not allowed.
//
// On reset, memory loads from file "loadfile_0.img",
// "loadfile_1.img", "loadfile_2.img", or "loadfile_3.img", depending
// on the "bank_id" input.
//
// File format:
//     @0
//     <hex data 0>
//     <hex data 1>
//     ...etc
//
// If input "create_dump" is true on rising clock,
// contents of memory will be dumped to
// file "dumpfile_0", "dumpfile_1", etc, depending on
// "bank_id".  It will dump from location 0 up through
// the highest location modified by a write.
//
// File names for loading and dumping is the only purpose of
// the "bank_id" input.  (You may change the name of the file
// in the $readmemh statement below.)
//
//////////////////////////////////////

module modded_memory (
    output [15:0] data_out,
    output        err,
    input  [15:0] data_in,
    input  [12:0] addr,
    input         wr,
    input         rd,
    input         enable,
    input   [1:0] bank_id,
    input         clk,
    input         rst
);

    reg     [7:0]  mem [0:32];

    wire [13:0] addr_1c;
    wire [15:0] data_in_1c;

    assign rd0 = rd & ~wr & enable & ~rst;
    assign wr0 = ~rd & wr & enable & ~rst;
   
    dflipflop ff0 (rd1, rd0, clk, rst);
    dflipflop ff1 (wr1, wr0, clk, rst);
    dflipflop reg0 [12:0] (addr_1c[12:0], addr, clk, rst);
    dflipflop reg1 [15:0] (data_in_1c, data_in, clk, rst);
    assign addr_1c[13]=1'b0;

    wire [15:0] data_out_1c = rd1 ? mem[addr_1c] : {16{1'bz}};

    dflipflop reg2 [15:0] (data_out, data_out_1c, clk, rst);

    dflipflop ff2 (rd2, rd1, clk, rst);
    dflipflop ff3 (wr2, wr1, clk, rst);
    dflipflop ff4 (rd3, rd2, clk, rst);
    dflipflop ff5 (wr3, wr2, clk, rst);

    assign busy = rd1 | rd2 | rd3 | wr1 | wr2 | wr3;
    assign err = ((rd0 | wr0) & busy) | (rd & wr & enable & ~rst);

    always @(posedge clk) begin
        if (wr1) begin
          mem[addr] = data_in_1c[15:0];
        end
    end


endmodule  // final_memory
// DUMMY LINE FOR REV CONTROL :0:
