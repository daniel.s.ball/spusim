/* $Author: karu $ */
/* $LastChangedDate: 2009-03-04 23:09:45 -0600 (Wed, 04 Mar 2009) $ */
/* $Rev: 45 $ */
/* Modified by KARL JOHN WALLINGER and SAMUEL LAWRENCE WASMUNDT 04/19/2011 */
/* To make similar to cache interface */
//////////////////////////////////////
//
// Memory -- stalling single cycle version
//
// written for CS/ECE 552, Spring '06
// Andy Phelps, 25 Jan 2006
//
// Added reading seed from command line args
// all mem is intialized to zero first
// 
// This is a byte-addressable,
// 16-bit wide, 64K-byte memory that allows aligned accesses only.
//
// This module produces a "ready" signal;
// if "ready" is not asserted, the read
// or write did not take place.
//
// Reads happen combinationally with zero delay in cycles that ready is high.
// Writes occur on rising clock edge in cycles that ready is high.
// Concurrent read and write not allowed.
//
// On reset, memory loads from file "loadfile_all.img".
// (You may change the name of the file in
// the $readmemh statement below.)
// File format:
//     @0
//     <hex data 0>
//     <hex data 1>
//     ...etc
//
// If input "createdump" is true on rising clock,
// contents of memory will be dumped to
// file "dumpfile", from location 0 up through
// the highest location modified by a write.
//
//
//////////////////////////////////////

module stallmem (DataOut, Done, Stall, CacheHit, err, Addr, DataIn, Rd, Wr, clk, rst);

   output  [15:0] DataOut;
   output         Done;
   output         Stall;
   output         CacheHit;
   input [15:0]   DataIn;
   input [15:0]   Addr;
   input          Wr;
   input          Rd;
   input          clk;
   input          rst;
   output         err;

   reg [7:0]      mem [0:63];
   reg            loaded;
   reg [16:0]     largest;
   reg [31:0]     rand_pat;

   wire           ready;
   wire muxSel;
   assign muxSel = ready & (~Wr);

   assign         ready = (Wr|Rd) & rand_pat[0];
   assign         Stall = (Wr|Rd) & ~rand_pat[0];
   assign         err = ready & Addr[0]; //word aligned; odd address is invalid
   assign         Done = ready;
   assign         DataOut = err ? 
			      ((ready & (~Wr))? {mem[Addr-8'h1],mem[Addr]}: 0) :
			      ((ready & (~Wr))? {mem[Addr],mem[Addr+8'h1]}: 0);
   assign         CacheHit = 0;

   always @(posedge clk) begin
      else begin
         rand_pat = (rand_pat >> 1) | (rand_pat[0] << 31);
		 #1;
         if (ready & Wr & ~err) begin
	        mem[Addr] = DataIn[15:8];       // The actual write
	        mem[Addr+1] = DataIn[7:0];    // The actual write
            if ({1'b0, Addr} > largest) largest = Addr;  // avoid negative numbers
         end
      end
   end


endmodule
// DUMMY LINE FOR REV CONTROL :0:
